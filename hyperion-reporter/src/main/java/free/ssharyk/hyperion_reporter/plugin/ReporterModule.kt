package free.ssharyk.hyperion_reporter.plugin

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity

import com.willowtreeapps.hyperion.plugin.v1.PluginModule

import free.ssharyk.hyperion_reporter.R
import free.ssharyk.hyperion_reporter.ui.ReportDialog

class ReporterModule : PluginModule(), View.OnClickListener {
    override fun createPluginView(layoutInflater: LayoutInflater, parent: ViewGroup): View? {
        val view = layoutInflater.inflate(R.layout.hyperion_reporter_item_plugin, parent, false)
        view.setOnClickListener(this)
        return view
    }

    override fun onClick(view: View) {
        ReportDialog().show((extension.activity as AppCompatActivity).supportFragmentManager, "Reporter")
    }
}