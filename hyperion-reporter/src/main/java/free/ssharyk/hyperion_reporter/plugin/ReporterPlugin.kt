package free.ssharyk.hyperion_reporter.plugin

import com.google.auto.service.AutoService
import com.willowtreeapps.hyperion.plugin.v1.Plugin
import com.willowtreeapps.hyperion.plugin.v1.PluginModule

@AutoService(Plugin::class)
class ReporterPlugin : Plugin() {
    override fun createPluginModule(): PluginModule? {
        return ReporterModule()
    }
}
