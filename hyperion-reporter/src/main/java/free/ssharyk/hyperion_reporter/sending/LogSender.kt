package free.ssharyk.hyperion_reporter.sending

import android.util.Log
import free.ssharyk.hyperion_reporter.info.Info

enum class LogLevel {
    INFO,
    ERROR
}

class LogSender(private val tag: String, private val level: LogLevel = LogLevel.INFO) : ISender {
    override val title: String = "Вывод в logcat"

    override fun send(infos: List<Info>) {
        val string = StringBuilder("Hyperion-Reporter Information\n")
        infos.forEach { info ->
            string.appendln("  ==> ${info.header}")
            info.data.forEach {
                string.appendln("    - \t${it.key} --> ${it.value}")
            }
        }

        writeToLog(string.toString(), level)
    }

    private fun writeToLog(string: String, level: LogLevel) {
        when (level) {
            LogLevel.INFO -> Log.i(this.tag, string)
            LogLevel.ERROR -> Log.e(this.tag, string)
            else -> Log.d(this.tag, string)
        }
    }
}