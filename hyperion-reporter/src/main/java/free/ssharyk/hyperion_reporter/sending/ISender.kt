package free.ssharyk.hyperion_reporter.sending

import free.ssharyk.hyperion_reporter.info.Info

interface ISender {
    val title: String
    fun send(infos: List<Info>)
}