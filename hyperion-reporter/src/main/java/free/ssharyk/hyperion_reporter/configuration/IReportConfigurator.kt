package free.ssharyk.hyperion_reporter.configuration

interface IReportConfigurator {
    val configuration: ReportConfiguration
}