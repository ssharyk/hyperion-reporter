package free.ssharyk.hyperion_reporter.configuration

import free.ssharyk.hyperion_reporter.infocollection.IInfoCollector
import free.ssharyk.hyperion_reporter.sending.ISender

data class ReportConfiguration(val infoCollectors: List<IInfoCollector>, val senders: List<ISender>)