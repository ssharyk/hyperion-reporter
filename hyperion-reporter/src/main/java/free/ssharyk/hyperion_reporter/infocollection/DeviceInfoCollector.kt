package free.ssharyk.hyperion_reporter.infocollection

import android.os.Build
import free.ssharyk.hyperion_reporter.info.Info

class DeviceInfoCollector : IInfoCollector {
    override val title: String = "Информация об устройстве"

    override fun collect(): Info {
        val data = mutableMapOf<String, Any>(
            "DEVICE" to Build.DEVICE,
            "MODEL" to Build.MODEL,
            "PRODUCT" to Build.PRODUCT,
            "SDK" to Build.VERSION.SDK_INT
        )
        return Info("Device", data)
    }
}