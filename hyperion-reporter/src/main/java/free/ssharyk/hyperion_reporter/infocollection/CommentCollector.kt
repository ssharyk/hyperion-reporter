package free.ssharyk.hyperion_reporter.infocollection

import free.ssharyk.hyperion_reporter.info.Info

class CommentCollector(private val comment: String) : IInfoCollector {
    override val title: String = "Комментарий"

    override fun collect(): Info {
        return Info(
            "User comment",
            mutableMapOf(
                "MESSAGE" to comment,
                "TIMESTAMP" to System.currentTimeMillis()
            )
        )
    }
}