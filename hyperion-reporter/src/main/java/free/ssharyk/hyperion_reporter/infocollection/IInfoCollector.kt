package free.ssharyk.hyperion_reporter.infocollection

import free.ssharyk.hyperion_reporter.info.Info

interface IInfoCollector {
    val title: String
    fun collect(): Info
}