package free.ssharyk.hyperion_reporter.infocollection

import android.content.Context
import free.ssharyk.hyperion_reporter.info.Info

class AppInfoCollector(private val context: Context) : IInfoCollector {
    override val title: String = "Информация о приложении"

    override fun collect(): Info {
        val packageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        val data = mutableMapOf<String, Any>(
            "PACKAGE_NAME" to packageInfo.packageName,
            "VERSION_ID" to packageInfo.versionCode,
            "VERSION_NAME" to packageInfo.versionName,
            "INSTALLATION_TIME" to packageInfo.firstInstallTime,
            "LAST_UPDATE_TIME" to packageInfo.lastUpdateTime
        )
        return Info("Application", data)
    }
}