package free.ssharyk.hyperion_reporter.info

data class Info(val header: String, val data: MutableMap<String, Any> = mutableMapOf()) {
    fun add(key: String, value: Any) {
        this.data[key] = value
    }
}