package free.ssharyk.hyperion_reporter.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import free.ssharyk.hyperion_reporter.R
import kotlinx.android.synthetic.main.hyperion_reporter_item_selection.view.*

class SelectAdapter(context: Context, private val items: List<SelectItem>) :
    RecyclerView.Adapter<SelectAdapter.SelectItemViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectItemViewHolder {
        val view = layoutInflater.inflate(R.layout.hyperion_reporter_item_selection, parent, false)
        return SelectItemViewHolder(view)
    }

    override fun getItemCount(): Int =
        items.size

    override fun onBindViewHolder(holder: SelectItemViewHolder, position: Int) {
        holder.bind()
    }

    val titles: List<String>
        get() = items.filter { it.isSelected }.map { it.title }

    inner class SelectItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val checkIsSelected: CheckBox = itemView.hyperion_reporter_selection_item_isSelected
        private val txvTitle: TextView = itemView.hyperion_reporter_selection_item_title

        fun bind() {
            val item = items[adapterPosition]
            txvTitle.text = item.title
            txvTitle.setOnClickListener {
                item.isSelected = !item.isSelected
                notifyItemChanged(adapterPosition)
            }

            checkIsSelected.isChecked = item.isSelected
            checkIsSelected.setOnCheckedChangeListener { _, isChecked ->
                item.isSelected = isChecked
            }
        }
    }
}