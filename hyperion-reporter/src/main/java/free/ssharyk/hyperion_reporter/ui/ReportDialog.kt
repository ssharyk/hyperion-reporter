package free.ssharyk.hyperion_reporter.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager

import free.ssharyk.hyperion_reporter.R
import free.ssharyk.hyperion_reporter.configuration.IReportConfigurator
import free.ssharyk.hyperion_reporter.configuration.ReportConfiguration
import free.ssharyk.hyperion_reporter.core.Reporter
import free.ssharyk.hyperion_reporter.infocollection.CommentCollector

import kotlinx.android.synthetic.main.hyperion_reporter_dialog.view.*

class ReportDialog : DialogFragment() {
    private lateinit var customView: View

    private lateinit var collectorsAdapter: SelectAdapter
    private lateinit var sendersAdapter: SelectAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        customView = layoutInflater.inflate(R.layout.hyperion_reporter_dialog, container, false)

        configureLists()
        configureListeners()

        return customView
    }

    private fun configureLists() {
        val configuration = this.configuration
        if (configuration == null) {
            customView.hyperion_reporter_lists.visibility = View.GONE
            return
        }

        val collectorsItems = configuration.infoCollectors.map { SelectItem(it.title) }
        customView.hyperion_reporter_lstCollectors.layoutManager = LinearLayoutManager(context)
        collectorsAdapter = SelectAdapter(context!!, collectorsItems)
        customView.hyperion_reporter_lstCollectors.adapter = collectorsAdapter
        customView.hyperion_reporter_lstCollectors.isNestedScrollingEnabled = false

        val senderItems = configuration.senders.map { SelectItem(it.title) }
        customView.hyperion_reporter_lstSenders.layoutManager = LinearLayoutManager(context)
        sendersAdapter = SelectAdapter(context!!, senderItems)
        customView.hyperion_reporter_lstSenders.adapter = sendersAdapter
        customView.hyperion_reporter_lstSenders.isNestedScrollingEnabled = false
    }

    private fun configureListeners() {
        customView.hyperion_reporter_send.setOnClickListener {
            configuration?.let {

                val selectedCollectors =
                    it.infoCollectors
                        .filter { coll -> collectorsAdapter.titles.contains(coll.title) }
                        .toMutableList()
                val comment = customView.hyperion_reporter_comment.text.toString()
                if (comment.isNotEmpty()) {
                    selectedCollectors.add(CommentCollector(comment))
                }

                val selectedSenders =
                    it.senders.filter { coll -> sendersAdapter.titles.contains(coll.title) }

                val newConfiguration = ReportConfiguration(selectedCollectors, selectedSenders)

                Reporter().report(newConfiguration)

                this@ReportDialog.dismiss()
            }
        }
    }

    private val configuration: ReportConfiguration?
        get() = (context?.applicationContext as? IReportConfigurator)?.configuration
}