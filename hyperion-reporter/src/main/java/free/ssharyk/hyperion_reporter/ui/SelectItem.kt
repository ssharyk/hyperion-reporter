package free.ssharyk.hyperion_reporter.ui

data class SelectItem(val title: String, var isSelected: Boolean = true)