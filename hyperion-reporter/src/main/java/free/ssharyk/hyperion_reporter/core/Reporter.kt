package free.ssharyk.hyperion_reporter.core

import free.ssharyk.hyperion_reporter.configuration.ReportConfiguration

class Reporter {
    fun report(configuration: ReportConfiguration) {
        val data = configuration.infoCollectors.map { it.collect() }

        configuration.senders.forEach {
            it.send(data)
        }
    }
}