package free.ssharyk.hyperion_recycler_attrs

import android.view.View
import android.view.ViewParent
import androidx.recyclerview.widget.RecyclerView
import com.google.auto.service.AutoService
import com.willowtreeapps.hyperion.attr.ViewAttribute
import com.willowtreeapps.hyperion.attr.collectors.TypedAttributeCollector
import com.willowtreeapps.hyperion.plugin.v1.AttributeTranslator

@AutoService(TypedAttributeCollector::class)
class ViewInHolderAttributeCollector : TypedAttributeCollector<View>(View::class.java) {

    override fun collect(view: View, attributeTranslator: AttributeTranslator?): List<ViewAttribute<String>> {

        val attributes = mutableListOf<ViewAttribute<String>>()

        val rv = findRecyclerViewOf(view)
        rv?.let {
            attributes.addAll(RecyclerViewAttributeObtainer(it).attributes)

            attributes.addAll(obtainViewHolderProperties(it, view))
        }

        return attributes
    }

    private fun findRecyclerViewOf(view: View): RecyclerView? {

        var parent: ViewParent? = view.parent
        while (parent != null) {
            if (parent is RecyclerView) {
                return parent
            }

            parent = parent.parent
        }

        return null
    }

    private fun obtainViewHolderProperties(recyclerView: RecyclerView, view: View): List<ViewAttribute<String>> {
        val viewHolder: RecyclerView.ViewHolder? = recyclerView.findContainingViewHolder(view)
        return if (viewHolder == null) {
            listOf(ViewAttribute("ViewHolder", "NULL VH"))
        } else {
            listOf(
                ViewAttribute("AdapterPosition", viewHolder.adapterPosition.toString()),
                ViewAttribute("ItemViewType", viewHolder.itemViewType.toString()),
                ViewAttribute("ItemId", viewHolder.itemId.toString()),
                ViewAttribute("ViewHolder", viewHolder.toString())
            )
        }
    }
}