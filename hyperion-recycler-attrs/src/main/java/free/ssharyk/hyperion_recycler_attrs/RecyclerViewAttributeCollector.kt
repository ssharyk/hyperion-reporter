package free.ssharyk.hyperion_recycler_attrs

import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.google.auto.service.AutoService
import com.willowtreeapps.hyperion.attr.collectors.TypedAttributeCollector
import com.willowtreeapps.hyperion.attr.ViewAttribute
import com.willowtreeapps.hyperion.plugin.v1.AttributeTranslator


@AutoService(TypedAttributeCollector::class)
class RecyclerViewAttributeCollector :
    TypedAttributeCollector<RecyclerView>(RecyclerView::class.java) {

    @NonNull
    override fun collect(view: RecyclerView, attributeTranslator: AttributeTranslator): List<ViewAttribute<String>> {
       return RecyclerViewAttributeObtainer(view).attributes
    }
}