package free.ssharyk.hyperion_recycler_attrs

import androidx.recyclerview.widget.RecyclerView
import com.willowtreeapps.hyperion.attr.ViewAttribute

internal class RecyclerViewAttributeObtainer(private val rv: RecyclerView) {
    val attributes: List<ViewAttribute<String>>
        get() {
            return mutableListOf(
                ViewAttribute("LayoutManager", rv.layoutManager?.javaClass?.simpleName ?: "NULL LAYOUT MANAGER"),
                ViewAttribute("Adapter", rv.adapter?.javaClass?.simpleName ?: "NULL ADAPTER"),
                ViewAttribute("FixedSize", rv.hasFixedSize().toString()),
                ViewAttribute("StableIDs", rv.adapter?.hasStableIds().toString()),
                ViewAttribute("ItemsCount", rv.adapter?.itemCount.toString())
            )
        }
}