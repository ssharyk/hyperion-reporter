package free.ssharyk.hyperionreporter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.willowtreeapps.hyperion.core.Hyperion
import free.ssharyk.hyperionreporter.plugins.PluginsAdapter
import free.ssharyk.hyperionreporter.plugins.PluginsRepository
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        lstPlugins.adapter = PluginsAdapter(this, PluginsRepository.get())
        lstPlugins.layoutManager = LinearLayoutManager(this)
    }
}
