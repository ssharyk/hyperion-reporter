package free.ssharyk.hyperionreporter

import android.app.Application
import free.ssharyk.hyperion_reporter.configuration.IReportConfigurator
import free.ssharyk.hyperion_reporter.configuration.ReportConfiguration
import free.ssharyk.hyperion_reporter.infocollection.AppInfoCollector
import free.ssharyk.hyperion_reporter.infocollection.DeviceInfoCollector
import free.ssharyk.hyperion_reporter.infocollection.IInfoCollector
import free.ssharyk.hyperion_reporter.sending.ISender
import free.ssharyk.hyperion_reporter.sending.LogSender

class App : Application(), IReportConfigurator {
    override val configuration: ReportConfiguration
        get() {
            val collectors: List<IInfoCollector> = listOf(DeviceInfoCollector(), AppInfoCollector(this))
            val senders: List<ISender> = listOf(LogSender("MY_APP"))
            return ReportConfiguration(collectors, senders)
        }
}