package free.ssharyk.hyperionreporter.plugins

object PluginsRepository {
    fun get(): List<Plugin> {
        return listOf(
            Plugin("Attributes", "Inspect views and adjust their attributes."),
            Plugin("Crash", "Show alternative activity when app crashes with the crash details. No UI for this module within drawer."),
            Plugin("Phoenix", "Clear local storage and relaunch the app."),
            Plugin("Recorder", "Record, save, and share a video of your app."),
            Plugin("Shared-Preferences", "View and edit your app's key-value storage."),

            Plugin("Reporter", "Send custom info with custom channels", PluginStatus.IN_DEVELOPMENT),
            Plugin("RV", "Obtain data about VHs in list", PluginStatus.IN_DEVELOPMENT)
        )
    }
}