package free.ssharyk.hyperionreporter.plugins

enum class PluginStatus {
    STABLE,
    IN_DEVELOPMENT,
    IDEA
}

data class Plugin(val name: String, val description: String, val status: PluginStatus = PluginStatus.STABLE)