package free.ssharyk.hyperionreporter.plugins

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import free.ssharyk.hyperionreporter.R
import kotlinx.android.synthetic.main.item_plugin.view.*

class PluginsAdapter(private val context: Context, private val plugins: List<Plugin>) :
    RecyclerView.Adapter<PluginsAdapter.PluginViewHolder>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PluginViewHolder {
        val view = layoutInflater.inflate(R.layout.item_plugin, parent, false)
        return PluginViewHolder(view)
    }

    override fun getItemCount(): Int = plugins.size

    override fun onBindViewHolder(holder: PluginViewHolder, position: Int) {
        return holder.bind(plugins[position])
    }

    override fun getItemId(position: Int): Long {
        return position.toLong() * (-10L)
    }


    inner class PluginViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val txvName: TextView = itemView.plugin_item_name
        private val txvDescription: TextView = itemView.plugin_item_description
        private val txvStatus: TextView = itemView.plugin_item_status

        fun bind(plugin: Plugin) {
            txvName.text = plugin.name
            txvDescription.text = plugin.description
            txvStatus.text = plugin.status.name
            val statusColor = when (plugin.status) {
                PluginStatus.STABLE -> R.color.colorBlack
                PluginStatus.IN_DEVELOPMENT -> R.color.colorRed
                PluginStatus.IDEA -> R.color.colorBlue
            }
            txvStatus.setTextColor(context?.resources.getColor(statusColor))
        }
    }
}